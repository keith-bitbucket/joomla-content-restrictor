<?php defined('_JEXEC') or die();
/**
 * @version		0.9.0
 * @package		Wf If
 * @subpackage	Plugin
 * @copyright	2011 Webfire, All rights reserved.
 * @license		GNU/GPL
 */

jimport('joomla.plugin.plugin');

/**
 * Webfire Content Restritor Plugin
 *
 * @package		Webfire Content Restritor
 * @subpackage	Plugin
 */
class plgContentWfif extends JPlugin {
    /**
     * Constructor
     *
     * @param	object	Plugin observer
     * @param	array	Plugin parameters
     */
    public function __construct(&$subject, $params) {
        parent::__construct($subject, $params);
    }

    /**
     * Filters content for unauthenticated users
     *
     * @param	object	Article object
     * @param	array	Article parameters array
     * @param	int		Pagination offset
     */
    public function onPrepareContent(&$article, &$params, $limitstart) {
        global $mainframe;

        // Load plugin parameters
        $component  = JComponentHelper::getComponent('com_wfif');

        // does this text contain any wf_link content?
        if (preg_match_all("|\[wf-if](.*?)\[wf-else](.*?)\[wf-fi]|", $article->text, $matches, PREG_PATTERN_ORDER)){

            // do we have our user_id yet?
            // $session = &JFactory::getSession();
            $session = &JSession::getInstance('none', array());
            $wfif_user_email = $session->get('wfif_user_email', null);

            if(empty($wfif_user_email)){

                // do we have our cookie?
                $wfif_user_email = $_COOKIE['wfif_user_email'];
                if(empty($wfif_user_email)){

                    // we need to prompt for information
                    $wfif_user_email = "";

                }

                $session->set('wfif_user_email', $wfif_user_email);

            }
            $exp = time() + 60*60*24*30;
            setcookie('wfif_user_email', $wfif_user_email, $exp, '/');

            // replace text
            for($i=0; $i<count($matches[0]); $i++){
                if(!empty($wfif_user_email)){
                    // grant access
                    $article->text = str_replace($matches[0][$i], $matches[2][$i], $article->text);
                }else{
                    $html = '<a class="modal linktext" rel="{size: {x: 325, y: 370}}" href="/wfif-form.php?url=' . urlencode($_SERVER['REDIRECT_URL']) . '">' . $matches[1][$i] . '</a>';
                    $article->text = str_replace($matches[0][$i], $html, $article->text);
                }
            }

        }
    }
}
