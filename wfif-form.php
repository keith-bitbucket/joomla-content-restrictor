date_default_timezone_set('America/Chicago');
$fa = $_POST['form'];
foreach($fa as $key => $val){
  $fa[$key] = str_replace('"','\\"',$val);
}
list($fn,$ln) = explode(" ", $fa['name'], 2);
$fa['send_packet'] = (strstr($fa['send_packet'],"Yes")!==FALSE ? "yes" : "no");

$fastxml = "";
$fastxml .= "<?xml version=\"1.0\" standalone=\"yes\"?>\n";
$fastxml .= "<RootNode>\n  <Lead>\n";
$fastxml .= "    <Contact LastName=\"{$ln}\" FirstName=\"{$fn}\" Email=\"{$fa['email_address']}\" Phone=\"{$fa['phone']}\" StreetAddress=\"{$fa['address']}\" City=\"{$fa['city']}\" State=\"{$fa['state']}\" PostalCode=\"{$fa['zip']}\" VisitDate=\"" .date("m/d/Y"). "\" />\n";
$fastxml .= "    <Qualifications InfoPacket=\"{$fa['send_packet']}\" ContactMethod=\"{$fa['contact_method']}\" Comments=\"{$fa['questions']}\" />\n";
$fastxml .= "    <PropertyInterest BuilderName=\"JBH\" MasterCommunity=\"1000\" CommunityName=\"web\" />\n";
$fastxml .= "  </Lead>\n</RootNode>\n";

$filename = time() . ".xml";
mail('keith@webfire.com',$filename,$fastxml);
